#!/bin/bash

# source: https://gist.github.com/francoisromain/58cabf43c2977e48ef0804848dee46c3
# and another script to delete the directories created by this script
# project-delete.sh: https://gist.github.com/francoisromain/e28069c18ebe8f3244f8e4bf2af6b2cb

# Call this file with `bash ./project-create.sh project-name`
# - project-name is mandatory

# This will creates 4 directories and a git `post-receive` hook.
# The 4 directories are:
# - $GIT: a git repo
# - $TMP: a temporary directory for deployment
# - $WWW: a directory for the actual production files
# - $ENV: a directory for the env variables

# When you push your code to the git repo,
# the `post-receive` hook will deploy the code
# in the $TMP directory, then copy it to $WWW.

DIR_TMP="/srv/tmp/"
DIR_WWW="/srv/www/"
DIR_GIT="/srv/git/"

if [ $# -eq 0 ]; then
	echo 'No project name provided (mandatory)'
	exit 1
else
	export APP_NAME="$1"
	echo "- Project name:" "$1"
fi


# install node
sudo apt update 
curl -sL https://deb.nodesource.com/setup_14.x | sudo bash -
sudo apt --assume-yes -y install nodejs
sudo apt --assume-yes -y install npm
sudo npm install -g pm2


# create directories
GIT=$DIR_GIT$1.git
TMP=$DIR_TMP$1
WWW=$DIR_WWW$1

echo "- git:" "$GIT"
echo "- tmp:" "$TMP"
echo "- www:" "$WWW"

export GIT
export TMP
export WWW


function dir_create() {
	sudo mkdir -p "$1"
	sudo chgrp -R users "$1" # set the group
	sudo chmod -R g+rwX  "$1" # allow the group to read/write
	sudo chmod g+s `find "$1" -type d` # new files get group id of directory
}

dir_create "$TMP" 
dir_create "$WWW" 
dir_create "$GIT" 

# Create a directory for the git repository
cd "$GIT" || exit

# Init the repo as an empty git repository
sudo git init --bare --shared=all 

cd hooks || exit

sudo touch post-receive

# create a post-receive file
sudo tee post-receive <<EOF
#!/bin/bash
# The app
APP_NAME="${APP_NAME}"
# The production directory
WWW="${WWW}"
# A temporary directory for deployment
TMP="${TMP}"
# The Git repo
GIT="${GIT}"
# Deploy the content to the temporary directory
sudo git --work-tree=$TMP --git-dir=$GIT checkout -f || exit
# Do stuffs, like npm install
cd $TMP || exit

npm install

# Replace the content of the production directory
# with the temporary directory
cd $WWW || exit
sudo rm -r .\/*

cd $TMP || exit
sudo mv .\/* $WWW || exit

#enter the production directory
cd $WWW || exit
npm run build

#stop and start the pm2 app
pm2 stop $APP_NAME
pm2 start server/server.js --name $APP_NAME
sudo systemctl restart nginx.service
EOF

# make it executable
sudo chmod +x post-receive

echo "$GIT"
